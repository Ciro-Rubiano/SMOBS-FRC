package servlets;

import logic.GestorEncolamiento;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;


@WebServlet(name = "PushServlet")
public class PushServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            String content_type = request.getHeader("Content-Type");
            if (content_type.equals("application/json")) {

                //get Body
                String body = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

                GestorEncolamiento.InsertInQueue(body);

                response.setStatus(200);
                PrintWriter writter = response.getWriter();
                writter.println("OK, el contenido fue procesado exitosamente");

            } else {
                response.setStatus(400);
                PrintWriter writter = response.getWriter();
                writter.println("Json mal formateado");
            }
        } catch (Exception ex) {
            response.setStatus(500);
            PrintWriter writter = response.getWriter();
            writter.println("Hubo un error procesando la solicitud" + ex.getMessage());
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
