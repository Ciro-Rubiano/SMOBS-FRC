package logic;

import com.google.gson.Gson;
import models.BodyContent;

import java.util.LinkedList;

public class GestorEncolamiento {

    private static LinkedList<BodyContent>[] colas;

    static{
        colas = GestorDePersistencia.deserialize();
    }

    public static void InsertInQueue(String message) {

        BodyContent body = new Gson().fromJson(message, BodyContent.class);

        if (colas[(body.getControladorID())] == null) {
            colas[(body.getControladorID())] = new LinkedList<>();
        }
        colas[(body.getControladorID())].add(body);
        GestorDePersistencia.serialize(colas);

    }

    public static String getMessage(int id) {

        String toreturn = colas[id] == null?"No hay mensajes...": colas[id].isEmpty() ?"No hay Mensajes...": colas[id].removeFirst().getJson();
        GestorDePersistencia.serialize(colas);
        return toreturn;

    }
}
