package smobs;


import jade.core.Agent;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BookSellerAgent extends Agent{
    
    private final Logger LOGGER = Logger.getLogger(this.getClass().toString());

    private HashMap<String, Integer> catalogo;

    private BookSellerGui myGui;

    protected void setup(){
        LOGGER.log(Level.INFO, "[INFO] Comenzando setup de" + getAID().getLocalName());
        catalogo = new HashMap<>();

        try{
            myGui = new BookSellerGui(this);
            //myGui.setVisible(true);
            myGui.showGui();
        }catch (Exception ex){
            LOGGER.log(Level.SEVERE, "############[EXCEPTION]\n", ex);
        }
        

        addBehaviour(new OfferRequestServer());

        addBehaviour(new PurchaseOrdersServer());


        /*
            Ahora vamos a registrar los comportamientos en las paginas amarillas,
            para que un buyer pueda encontrar los servicios requeridos
        */
        //registramos los Behaviours en las Paginas Amarillas (DF)
        DFAgentDescription descAgente = new DFAgentDescription();
        descAgente.setName(getAID());
        //Creamos la descripcion del servicio
        ServiceDescription descServicio = new ServiceDescription();
        descServicio.setName("jade.book.trading");
        descServicio.setType("book.selling");
        //lo añadimos a la descripcion del agente
        descAgente.addServices(descServicio);

        try {
            //ahora lo agregamos a las paginas amarillas
            DFService.register(this, descAgente);//este objeto, este descriptor

        } catch (FIPAException ex) {
            LOGGER.log(Level.SEVERE, "[EXCEP] Fallo al registrar en paginas amarillas");
            LOGGER.log(Level.SEVERE, ex.toString());
        }

               
    }

    @Override
    protected void takeDown(){
        myGui.dispose();
        
        LOGGER.log(Level.INFO, "Terminating...", getAID().getName());
        
        try {
            //removemos los servicios de las paginas amarillas para que no quede desreferenciado
            DFService.deregister(this);
        } catch (FIPAException ex) {
            LOGGER.log(Level.SEVERE, "[EXEP] No fue posible des-registrar al agente");
            LOGGER.log(Level.SEVERE, ex.toString());
        }
        
    }

    public void updateCatalogue(final String title, final int price){

        addBehaviour(new OneShotBehaviour(){
            @Override
            public void action(){
                catalogo.put(title, price);
            }
        });
    }
    
    private class OfferRequestServer extends CyclicBehaviour{

        @Override
        public void action() {
            
            /*
            En la siguiente instruccion especificamos una template de un mensaje
            esto quiere decir que vamos a decirle a "myAgent" que solo reciba mensajes
            de tipo X.
            
            CFP = CallForProposal
            */
           
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            
            //Capturamos un mensaje de la queue
            ACLMessage msg = myAgent.receive();

            //Controlamos que haya mensajes en la queue
            if(msg == null){
                block();//con block bloqueamos la ejecución del comportamiento hasta que lleguen nuevos mensajes
                return;
            }

            //Procesamos el mensaje
            String titulo = msg.getContent();

            //Creamos el mensaje de respuesta
            ACLMessage reply = msg.createReply();

            /*
            En la siguiente linea podemos obtener el catalogo de el mismo agente
            siempre y cuando estemos declarando esta instruccion en una clase
            PRIVADA (interna) DENTRO de la clase agente.
            */
            Integer precio = catalogo.get(titulo); 
            
            if(precio != null){
                
                /*
                especificamos la perform y seteamos el precio en el msg de
                respuesta
                */
                reply.setPerformative(ACLMessage.PROPOSE);
                reply.setContent(precio.toString());
                
                LOGGER.log(Level.INFO, "Precio encontrado, enviando a al Buyer");
                        
            }else{
                
                reply.setPerformative(ACLMessage.REFUSE);
                reply.setContent("Libro fuera de catalogo");
                
                LOGGER.log(Level.INFO, "No se encontró ningun libro en el catalogo. Enviando Refuse");
                
                
            }
            
            myAgent.send(reply);
            
            LOGGER.log(Level.INFO, "Reply enviada");
        }

    }//Fin InnerClass 

    /**
     * La siguiente InnerClass provee el servicio de venta del libro propiamente
     */
    private class PurchaseOrdersServer extends CyclicBehaviour{

        @Override
        public void action() {
            //creamos una template para recibir cierto mensaje
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
            ACLMessage msg = myAgent.receive(mt);
            
            //en este punto tenemos el mensaje filtrado (si es que tenemos mensaje)
            
            if(msg!=null){
                //procesamos entonces el mensaje
                String titulo = msg.getContent();
                
                //retiramos del catalogo el producto
                Integer precio = catalogo.remove(titulo);
                
                //creamos la respuesta para el comprador
                ACLMessage reply = msg.createReply();
                
                //como puede que el libro ya no esté en el catalogo, vamos a validar que haya un precio del POP de la HashMap
                if(precio!= null){
                    reply.setPerformative(ACLMessage.INFORM);
                    reply.setContent("Compra Correcta");
                    LOGGER.log(Level.INFO, "[INFO] Libro procesado, venta realizada, informando a BuyerAgent");
                    
                }else{
                    reply.setPerformative(ACLMessage.FAILURE);//esto es si accedemos al catalogo y el libro ya no estaba
                    reply.setContent("El libro ya no se encuentra disponible");
                    LOGGER.log(Level.WARNING, "[WARNING] No se encontró el libro especificado, FAILURE enviado a BuyerAgent");
                }
                
                myAgent.send(reply);
            }else{
                block();
            }

        }
    
        
    
    }
    
    
}



