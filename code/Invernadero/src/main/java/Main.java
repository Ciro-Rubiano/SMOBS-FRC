import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.util.leap.Properties;
import jade.wrapper.AgentContainer;
import jade.wrapper.StaleProxyException;

public class Main {

    public static void main2(String[] args) {
        System.out.println(OntologySMOBS.MEDIR_HUMEDAD);
    }


    public static void main(String[] args) throws InterruptedException {
        //codigo copiado>>>>>>>
        Properties pp = new Properties();
        pp.setProperty(Profile.GUI, Boolean.TRUE.toString());
        Profile p = new ProfileImpl(pp);
        AgentContainer ac = jade.core.Runtime.instance().createMainContainer(p);
        //<<<<<<<<



        /*while(true){
             int rta = JOptionPane.showConfirmDialog(null, "Ready?", "Confirm", JOptionPane.YES_NO_OPTION);
             if(rta == 0)break;
             Thread.sleep(10000);

        }*/
        try {
            //Crear agente
            Agent comunicador = new ComunicadorAgente();
            Agent comunicador2 = new ComunicadorAgente();
            Agent humedad = new HumedadSueloAgente();
            Agent temperatura = new TemperaturaAgente();


            //iniciar agente
            ac.acceptNewAgent("SNIFFER", new jade.tools.sniffer.Sniffer()).start();
            //ac.acceptNewAgent("DUMMY-PUTO", new jade.tools.DummyAgent.DummyAgent()).start();
            ac.acceptNewAgent("Comunicador-1", comunicador).start();
            ac.acceptNewAgent("Comunicador-2", comunicador2).start();
            ac.acceptNewAgent("Humedad Suelo", humedad).start();
            ac.acceptNewAgent("Temperatura ", temperatura).start();


        } catch (StaleProxyException e) {
            throw new Error(e);
        }
    }

}
