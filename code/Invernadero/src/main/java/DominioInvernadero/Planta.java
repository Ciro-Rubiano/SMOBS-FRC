/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DominioInvernadero;

import java.util.ArrayList;

/**
 * @author Exe
 */
public class Planta {
    public String nombre;
    public String especie;
    public ArrayList<Medicion> mediciones;


    public void RegistrarMedicion(float medido, String fecha, String tipo) {
        for (Medicion m : mediciones) {
            if (m.tipo == tipo) {
                m.RegistrarMedicion(medido, fecha);

            }
        }

    }

    public void AgregrarMedicion(float medido, String fecha, String tipo) {
        Medicion medicion = new Medicion(medido, fecha, tipo);
        mediciones.add(medicion);
    }

}
