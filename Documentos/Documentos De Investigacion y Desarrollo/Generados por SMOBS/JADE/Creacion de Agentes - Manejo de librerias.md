# Creación de Agentes - Manejo de Librerías

> Fecha: 12/07
>
> Autores: Exequiel Salinas y Ciro Rubiano



[TOC]

---

## Introducción Teórica

Este es el primer documento relacionado a agentes, y JADE. 

Buscamos realizar un “hola mundo” usando Jade. 

Bien, partiendo de esto, nos es necesario describir que es Jade (o por lo menos que creemos que es). Jade es una librería, un conjunto de clases ya programadas en Java, que permiten realizar y ejecutar programas bajo metodologías orientadas a agentes. Cabe destacar que al ser java la programación orientada a objetos es inevitable y esto no es malo, dado que el Paradigma orientado a Agentes se puede entender como una particularidad del orientado a objetos

Para arrancar hay que sacarse de la cabeza la idea de que un agente se comporta como un objeto que solo tiene responsabilidades que cumplir. Los agentes funcionan de manera asíncrona y no tienen porque hacer lo que se les pide (esto no es literalmente así, pero se aproxima), ellos implementan comportamientos que vendrian siendo Metodos pero con alto nivel proactividad. 

Volviendo a Jade, esta librería nos es provista en un `.jar` que no tiene clase principal dentro de `MANIFEST.xml`  es decir, no es posible ejecutarla directamente. 

Otra cosa que descubrimos es que los agentes **no pueden ejecutarse sin contexto**, esto quiere decir, que no hay una “clase *main*“ que cree un agente y le pida cosas. 

Aproximándonos mas al código un agente extiende desde `jade.core.Agent` , los agentes **no tienen constructor** (o al menos por ahora no tiene alguna razón de existir). En su lugar se usa un método llamado `setup()` quien configura al agente en su creación.

Llegados a este punto creamos un primer agente llamado `HelloAgent.java`, a continuación el código:

```java
import jade.core.Agent;
/**
 *
 * @author CYRO
 */

public class HelloAgent extends Agent{
    
    protected void setup(){
        
        System.out.println("Hola Mundo. ");
        System.out.println("Mi nombre es: " + getLocalName());
    }
   
}
```

como se puede ver en la línea 12 encontramos un método `getLocalName()` que es responsabilidad del mismo Agent responder.
El nombre completo de un agente se compone de un **(local name)**@ **(platform name)** y ademas puede incluir direcciones utiles para comunicarnos con el agente.
El local name se lo damos al crear el agente, el platform name viene dado por la plataforma a la que pertenece y por defecto estara constitudio por **Host:Puerto**


Ok, pero como ejecutamos esto?

Bien, lo primero que hay que comprender es que es una **Plataforma**, un **Contenedor**  y un **Agente**. Lo que es un agente mas o menos lo explicamos recién, ahora la diferencia entre Plataforma y Contenedor es que uno es una <u>separación Lógica</u> entre agentes(Plataforma) y el contenedor es una <u>separación a nivel físico, son cada una de las maquinas que alberguen un agente .</u> Los agentes de una misma plataforma pueden comunicarse usando solo su **local name** , mientras que para distntas plataformas tendran que usar el **local name@platform name**. Una misma plataforma, va a poder contener varios contenedores y cada uno de estos múltiples agentes.

Bueno, entonces para darle vida a nuestro “HelloAgent” tuvimos que primero entender como dar contexto a los agentes, es decir, crear una Plataforma y un contenedor. Para ello trabajamos desde linea de comandos de windows (Win + R → cmd). 

> **NOTA:** encontramos problemas (graves) a la hora de hacer andar los siguientes pasos utilizando *PowerShell* de Windows. Ya que hay incompatibilidades  con los caracteres especiales (;).



### Lanzar entorno de Jade

[Download Jade 4.5.0](https://jade.tilab.com/dl.php?file=JADE-all-4.5.0.zip)

Para poder visualizar **jade** en ejecución podemos hacerlo con el siguiente comando:

```powershell
java jade.Boot -gui
```

Para que este comando funcione necesitamos tener configurado nuestro *classpath*. Podemos configurarlo desde la *cmd* de windows, o bien especificarlo en el mismo comando, de la siguiente manera:

```powershell
java -classpath ./lib/jade.jar jade.Boot -gui
```

Luego de ejecutar estos comandos deberíamos ver una interfaz grafica similar a esta:

![1563147223471](M:\Documentos\Facultad\SMOBS-FRC\Documentos\Investigaciones\JADE\Creacion de Agentes - Manejo de librerias.assets\1563147223471.png)

Allí podremos explorar las *plataformas* y *contenedores* como también los agentes involucrados, simplemente navegando por el árbol de la izquierda.



> *Nota:* La interfaz grafica de Jade tarda un tiempo en lanzarse



---

### Compilación de un agente

Entonces comenzamos compilando nuestra clase `HelloAgent.java`, para ello necesitamos compilarlo con el java compiler (`javac`) haciendo uso de librerías de jade. Ya que si intentamos compilarlo sin usar las librerías nos encontraríamos con un error de importaciones (recordar que nuestro código tiene la instrucción `import jade.core.Agent;`).

Entonces usamos el siguiente comando:

```powershell
javac -cp ./jade.jar -d ./classes ./source/HelloAgent.java
```

Los parámetros serian:

```powershell
javac -cp <<ruta Librerias para usar>> -d <<directorio destino>> <<clase a compilar>>
```

Como resultado de esta operación obtendríamos la clase ya compilada (`./classes/HelloAgent.class`)

### Ejecución en Jade del Agente compilado

Ahora usaremos nuestra clase ya compilada para añadirla a la plataforma de jade anteriormente mostrada. Para ello, haremos uso nuevamente de la interfaz grafica provista para mayor claridad.

Entonces vamos a agregar nuestro agente, en el mismo momento que estamos montando la plataforma.

```powershell
java -cp ./jade.jar;classes jade.Boot -gui -agents nuevoAgente:HelloAgent
```

Visto desde los parámetros seria algo como:

```powershell
java -cp <<librerias>>;<<ruta de clases a usar>> jade.Boot -gui -agents <<nombreLocal>>:<<NombreClase>>
```



Como resultado de esta acción deberíamos obtener un resultado por consola de la siguiente manera:

![1563149326813](M:\Documentos\Facultad\SMOBS-FRC\Documentos\Investigaciones\JADE\Creacion de Agentes - Manejo de librerias.assets\1563149326813.png)

además de abrir la interfaz grafica propiamente como mostramos [antes](#Lanzar entorno de Jade)


## Jade Tutorial ejemplo bookTrading 

Este tutrorial se encuentra cargado en el trello @JADE  

---

#### Links y Bibliografía: 

- [Classpath](http://jade.tilab.com/pipermail/jade-develop/2009q2/014003.html)
- [Tutoriales Jade](https://jade.tilab.com/documentation/tutorials-guides/jade-administration-tutorial/starting-jade/)
- [Formas de montaje](https://www.iro.umontreal.ca/~vaucher/Agents/Jade/primer1.html)
- [Formas de Montaje 2](https://www.iro.umontreal.ca/~vaucher/Agents/Jade/primer2.html)
- [Página oficial Jade](https://jade.tilab.com/)



