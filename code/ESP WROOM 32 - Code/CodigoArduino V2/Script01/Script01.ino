#include <HTTPClient.h>

#include <ETH.h>
#include <WiFi.h>
#include <WiFiAP.h>
#include <WiFiClient.h>
#include <WiFiGeneric.h>
#include <WiFiMulti.h>
#include <WiFiScan.h>
#include <WiFiServer.h>
#include <WiFiSTA.h>
#include <WiFiType.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>

 
const char* ssid = "Varsavsky";
const char* password =  "crypto256";
const int PIN_BLINK = 2;
 
void setup() {
 
  Serial.begin(115200);
  delay(4000);   //Delay needed before calling the WiFi.begin
 
  WiFi.begin(ssid, password); 
 
  while (WiFi.status() != WL_CONNECTED) { //Check for the connection
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
 
  Serial.println("Connected to the WiFi network");


  //DECLARACION DE PIN
  
  pinMode(PIN_BLINK, OUTPUT);

 
}

void loop(){
  HTTPClient http;
 
  http.begin("http://192.168.100.2:8282/smobsAPI/test"); //Specify destination for HTTP request
  http.addHeader("Content-Type", "text/plain");
  int httpResponseCode = http.POST("POSTING from ESP32");
  Serial.println(httpResponseCode);

  String result = http.getString();

  Serial.println(result);

  blink(result.toInt());

}


void blink(int tickrate){
  digitalWrite(PIN_BLINK, HIGH);
  delay(tickrate);           
  digitalWrite(PIN_BLINK, LOW);   
  delay(150); 
}

JsonObject readJson(String json){
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& obj = jsonBuffer.parseObject(json);
  if(!obj.success()){
    Serial.println("Hubo un error en el parseo de un JSONOBJECT");
  }

}