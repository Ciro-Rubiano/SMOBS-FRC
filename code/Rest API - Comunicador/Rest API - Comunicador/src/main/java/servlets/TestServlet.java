package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "TestServlet")
public class TestServlet extends HttpServlet {
    private static int acc = 2000;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        acc -= 200;
        if (acc > 0) response.getWriter().print(acc);
        else {
            response.getWriter().print(10);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.getWriter().print(request.getParameter("p"));

    }
}
