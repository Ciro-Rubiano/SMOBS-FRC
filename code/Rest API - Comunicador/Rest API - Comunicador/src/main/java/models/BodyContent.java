package models;

import com.google.gson.GsonBuilder;

import java.io.Serializable;

public class BodyContent implements Serializable {

    private int controladorID;
    private MessageParameter parametros;
    private String funcion;

    public String getFuncion() {
        return funcion;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public int getControladorID() {
        return controladorID;
    }

    public void setControladorID(int controladorID) {
        this.controladorID = controladorID;
    }

    public MessageParameter getParametros() {
        return parametros;
    }

    public void setParametros(MessageParameter parametros) {
        this.parametros = parametros;
    }

    public String getJson() {

        return new GsonBuilder().disableHtmlEscaping().create().toJson(this, this.getClass());

    }

    //INNERCLASS --------------------------------------
    public class MessageParameter implements Serializable {
        private String p1;
        private String p2;
        private String p3;
        private String p4;
        private String p5;

        public String getP1() {
            return p1;
        }

        public void setP1(String p1) {
            this.p1 = p1;
        }

        public String getP2() {
            return p2;
        }

        public void setP2(String p2) {
            this.p2 = p2;
        }

        public String getP3() {
            return p3;
        }

        public void setP3(String p3) {
            this.p3 = p3;
        }

        public String getP4() {
            return p4;
        }

        public void setP4(String p4) {
            this.p4 = p4;
        }

        public String getP5() {
            return p5;
        }

        public void setP5(String p5) {
            this.p5 = p5;
        }
    }//END OF INNERCLASS ----------------------------------------

}



