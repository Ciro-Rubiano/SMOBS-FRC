/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DominioInvernadero;

/**
 * @author Exe
 */
public class Medicion {

    public String getFechayHora() {
        return FechayHora;
    }

    public void setFechayHora(String FechayHora) {
        this.FechayHora = FechayHora;
    }

    public float getMedicion() {
        return medicion;
    }

    public void setMedicion(float medicion) {
        this.medicion = medicion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Medicion(float medicion, String FechayHora, String tipo) {
        this.FechayHora = FechayHora;
        this.medicion = medicion;
        this.tipo = tipo;
    }

    public void RegistrarMedicion(float medicion, String fecha) {
        setMedicion(medicion);
        setFechayHora(fecha);
    }

    public String FechayHora;
    public float medicion;
    public String tipo;


}

