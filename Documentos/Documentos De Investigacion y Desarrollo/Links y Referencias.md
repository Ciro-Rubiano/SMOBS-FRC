## Modalidad de Posteo por Tabla Respetar la estructura de Ejemplo

· Encerrar los campos entre Pipelines

. Para Control propio y del grupo de trabajo, escribir Su sigla una vez leido un link en la columna "Leido Por", la tabla de siglas se encuentra debajo del todo 

## Links Hardware 



|Pagina|Descripcion|URL|Leido Por|
|------|-----------|---|-|-|-|-|-|-|-|-|-|
|Wikipedia|Enciclopedia Libre|www.wikipedia.org|CY-EX-AX|
|ChatBot| Que es un FPGA y su importancia a futuro | https://planetachatbot.com/qu%C3%A9-es-una-fpga-y-por-qu%C3%A9-jugar%C3%A1n-un-papel-clave-en-el-futuro-e76667dbce3e|-|


## Links Software 
|Pagina|Descripcion|URL|Leido Por|
|------|-----------|---|---------|
|ResearchGate | Protocolos IEFT para IOT| https://www.researchgate.net/publication/319186036_Protocolos_IETF_para_IoT|-|
|Wikipedia | Paradigma Orientado a Agentes y Leng. de programación| https://en.wikipedia.org/wiki/Agent-oriented_programming |Ex-AX-LB|
|SARL|General-purpose Agent-Oriented Programming Language, con Java Interoperability Y Open Source |http://www.sarl.io/|Ex-AX|
|JADE|Agent Development Framework, an open source platform for peer-to-peer agent based applications | http://jade.tilab.com | Ex-LB|
|UNLP|Metodologias de desarrollo Multi-agente| http://sedici.unlp.edu.ar/bitstream/handle/10915/22566/Documento_completo.pdf?sequence=1| JV-AX-LB|
|OpenCV|Librerias para Vision Artificial|https://docs.opencv.org/3.3.1/|CY|
|Youtube| Video Explicativo sobre Arquitecturas Reactivas | https://www.youtube.com/watch?v=AZcFn2XPN3o|Ex-AX|
|ASME|AGENT SYSTEM ENGINEERING METHODOLOGY| http://aseme.tuc.gr/index.html|Ex|
|Aprendemachinelearning|Crear una Red Neuronal en Python desde cero|http://www.aprendemachinelearning.com/crear-una-red-neuronal-en-python-desde-cero/| JV|
|Aprendemachinelearning|Algoritmos de aprendizaje profundo|http://www.aprendemachinelearning.com/aprendizaje-profundo-una-guia-rapida/|JV|
|Eprints| Reconocimiento de Imagenes utilizando Redes Neuronales | https://eprints.ucm.es/23444/1/ProyectoFinMasterPedroPablo.pdf|AX|
|Wikipedia| Arquitectura dirigida por modelos | https://es.wikipedia.org/wiki/Arquitectura_dirigida_por_modelos| Ex-LB|
|NetLogo|Lenguaje y entorno de Programación Orientada a Agentes|http://ccl.northwestern.edu/netlogo/|LB|
|YouTube|Agent-Based Modeling and Multiagent Systems using NetLogo|https://www.youtube.com/playlist?list=PLSx7bGPy9gbHivKzRg2enzdABgKUd3u-E|LB|
|NetLogo|Links de descarga para distintos SOs|https://ccl.northwestern.edu/netlogo/6.0.4/|LB|
|Universidad De Sevilla|Introducción a la programación en NetLogo|https://www.cs.us.es/~fran/varios/maes.pdf|LB|
|NetLogo Web|Intérprete Web|http://www.netlogoweb.org/launch|LB|
|Hormigas NetLogo|Simulación de hormigas recolectando comida en NetLogo|https://goo.gl/yJFdde|LB|

## Links de Interes 

|Pagina|Descripcion|URL|Leido Por|
|------|-----------|---|-|-|-|-|-|-|-|-|-|
|SASE|Tutoriales y Workshops Descriptos |http://www.sase.com.ar/2018/files/2018/08/programa-sase-2018.pdf|CY-EX|
|Youtube| Red neuronales | https://www.youtube.com/watch?v=Ih5Mr93E-2c&hd=1| JV|


## Siglas
Sigla |Nombre y Apellido|
|--|--------------------|
LM | Lucas Madrid
LB | Lucio Bottacin
LE | Leo Marchiori
EX | Exequiel Salinas
CY | Ciro Rubiano
GS | Gaspar Spalla
AX | Axel Villegas
MP | Martin Polliotto
JV | Joaquin Velasco
