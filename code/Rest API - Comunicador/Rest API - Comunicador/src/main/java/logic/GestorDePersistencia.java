package logic;

import models.BodyContent;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public abstract class GestorDePersistencia {

    private static final String filename = "queuesSerialized.bin";
    private static FileOutputStream fout;
    private static FileInputStream fin;
    private static ObjectOutputStream oos;
    private static ObjectInputStream ois;


    public static void serialize(LinkedList<BodyContent>[] obj){
        try {
            fout = new FileOutputStream(filename, false);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(obj);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            System.out.println("No fue posible grabar el objeto");
        }
    }

    public static LinkedList<BodyContent>[] deserialize(){
        try{
            fin = new FileInputStream(filename);
            ois = new ObjectInputStream(fin);
            LinkedList<BodyContent>[] lista = (LinkedList<BodyContent>[]) ois.readObject();
            return lista;
        } catch (IOException e) {
            File file = new File(filename);
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            serialize(new LinkedList[100]);
            return deserialize();
        } catch (ClassNotFoundException e) {
            System.out.println("No fue posible recuperar el objeto del archivo");
        }
        return null;
    }


}
