import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;

public class HumedadSueloAgente extends Agent {

    private int tickrate = 45000; //milisecs
    private AID[] comunicadores;


    // variable que Lleva control de cada planta 


    @Override
    protected void setup() {

        //TODO checkear si es 1 o varios comunicadores


        //Publicacion del metodo en el DF
        DFAgentDescription temp_agent = new DFAgentDescription();
        ServiceDescription temp_service = new ServiceDescription();
        temp_service.setName("Medir.HumedadSuelo");
        temp_agent.addServices(temp_service);


        try {
            DFService.register(this, temp_agent);
        } catch (FIPAException fe) {
            fe.printStackTrace();

        }

        AgregarTickerBehaviourMedir();
    }


    public void AgregarTickerBehaviourMedir() {

        addBehaviour(new TickerBehaviour(this, tickrate) {
            @Override
            protected void onTick() {
                // cenvendra haecr un metodo statico para esto ?   
                //Encontrar comunicador

                //template para buscar comunicador 
                DFAgentDescription temp_agent = new DFAgentDescription();
                ServiceDescription temp_service = new ServiceDescription();

                temp_service.setType("Comunicacion Microcontrolador");
                temp_agent.addServices(temp_service);

                try {
                    DFAgentDescription[] encontrados = DFService.search(myAgent, temp_agent);
                    comunicadores = new AID[encontrados.length];
                    for (int i = 0; i < encontrados.length; ++i) {
                        System.out.println(encontrados[i]);
                        comunicadores[i] = encontrados[i].getName();
                        System.out.println(comunicadores[i]);

                    }
                } catch (FIPAException fe) {
                    fe.printStackTrace();
                }

                //¿debo medir?
                boolean debo_medir = true; //Logic

                if (debo_medir) {
                    //Creo un comportamiento para enviar un mensaje al comunicador solicitanco la medicion
                    myAgent.addBehaviour(new RequestHumedadSuelo());

                }
            }
        });
    }

    private class RequestHumedadSuelo extends Behaviour {
        private int paso = 0;
        private MessageTemplate mt;

        // Ahora usamos el lenght de agreeds  + refuseds private 
        //int contadorRtas = 0 ;


        // Averiguar por que se usan AIDS en los comunicadores ,o es mas practico obtener el .getName():String

        private ArrayList<AID> agreeds = new ArrayList<AID>();
        private ArrayList<AID> refuseds = new ArrayList<AID>();
        private ArrayList<AID> informs = new ArrayList<AID>();
        private ArrayList<AID> failures = new ArrayList<AID>();

        @Override
        public void action() {


            switch (paso) {
                case 0:
                    ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
                    for (int i = 0; i < comunicadores.length; ++i) {
                        request.addReceiver(comunicadores[i]);
                    }
                    request.setOntology(OntologySMOBS.MEDIR_HUMEDAD_SUELO.toString());
                    // Numero de microcontrolador/ plata or whaterver a medir 
                    //request.setContent(targetBookTitle);
                    request.setConversationId(OntologySMOBS.MEDIR_HUMEDAD_SUELO.toString());
                    request.setReplyWith("request" + System.currentTimeMillis()); // Unique value
                    myAgent.send(request);

                    // template para levantar respuestas 
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId(OntologySMOBS.MEDIR_HUMEDAD_SUELO.toString()), MessageTemplate.MatchInReplyTo(request.getReplyWith()));


                    paso = 1;
                    break;
                case 1:

                    ACLMessage rta = myAgent.receive(mt);
                    if (rta != null) { // respuesta recibida
                        System.out.println("Respuesta recibida");
                        if (rta.getPerformative() == ACLMessage.AGREE) {
                            System.out.println("Agreed");

                            agreeds.add(rta.getSender());

                        } else {
                            if (rta.getPerformative() == ACLMessage.REFUSE) {
                                System.out.println("Refused");
                                refuseds.add(rta.getSender());

                            }
                        }
                        if ((agreeds.size() + refuseds.size()) >= comunicadores.length) { // tengo todas las respuestas
                            System.out.println("Refuseds Y agreeds Recibidos ");
                            paso = 2;
                        }
                    } else {
                        block();
                    }
                    break;

                case 2:
                    rta = myAgent.receive(mt);
                    if (rta != null) { // respuesta  de la ejecucion recibida
                        System.out.println("Resultado recibido ");
                        if (rta.getPerformative() == ACLMessage.INFORM) {
                            informs.add(rta.getSender());
                            //Esto en vez de imprimirlo hay que asignarcelo a un objeto planta con el estado del la medicion.
                            System.out.println(rta.getContent());
                        } else {
                            if (rta.getPerformative() == ACLMessage.FAILURE) {
                                failures.add(rta.getSender());
                                // Enviar esto a un agente que vea que paso
                                System.out.println("ERROR " + rta.getContent());
                            }
                            //agregar un agreeds , luego comparar la cantidad de respuestas con los agreeds y pasar al paso siguiente , matar el behaviour

                        }
                        if ((failures.size() + informs.size()) >= agreeds.size()) { // tengo todas las respuestas de quienes me confirmaron
                            System.out.println("informs y failures recibidos ");
                            paso = 3;
                        }
                    } else {
                        block();
                    }
                    break;

            }
        }


        @Override
        public boolean done() {
            if (refuseds.size() >= comunicadores.length) {
                System.out.println("no se pudo realizar la medicion todos repondieron : Refused");
            } else if ((informs.size() + failures.size()) == agreeds.size())
                System.out.println("Finalizado , al menos un agreed");
            return (paso == 3);

        }

    }

}
