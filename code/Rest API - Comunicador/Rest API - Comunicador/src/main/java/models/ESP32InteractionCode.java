package models;

public enum ESP32InteractionCode {

    GoToSleep(0),
    HaveMessages(1),
    Error(-1),
    Reboot(8),
    NotUnderstand(9);


    private final int value;

    ESP32InteractionCode(int i) {
        //constructor
        this.value = i;
    }

    public String value() {
        return Integer.toString(value);

    }
}
