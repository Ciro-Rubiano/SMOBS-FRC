package servlets;

import logic.GestorEncolamiento;
import models.ESP32InteractionCode;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "PullServlet")
public class PullServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getHeader("idControlador") == null) {
            response.setStatus(400);//Bad Request
            response.getWriter().print("No se encontro la cabecera con el Key: \"idControlador\"");
        } else {




            String message = null;
            try {
                int idControlador = Integer.parseInt(request.getHeader("idControlador"));
                message = GestorEncolamiento.getMessage(idControlador);
            } catch (Exception ex) {
                response.setStatus(500);
                response.getWriter().print(ex.getMessage());
            }

            if (message == null) {
                //no tiene mensajes
                response.setStatus(200);
                response.addHeader("Content-Type", "application/json");
                response.addHeader("Interaction-Code", ESP32InteractionCode.GoToSleep.value());

            } else {
                //tiene mensajes
                response.setStatus(200);
                response.addHeader("Content-Type", "application/json");
                response.addHeader("Interaction-Code", ESP32InteractionCode.HaveMessages.value());
                response.getWriter().print(message);

            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
