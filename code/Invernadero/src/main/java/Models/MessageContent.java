package Models;

public class MessageContent {

    private String controladorID;
    private MessageParameter parametros;
    private String messageACLID;


    public String getControladorID() {
        return controladorID;
    }

    public MessageParameter getParametros() {
        return parametros;
    }

    public void setParametros(MessageParameter parametros) {
        this.parametros = parametros;
    }

    public String getMessageACLID() {
        return messageACLID;
    }

    public void setMessageACLID(String messageACLID) {
        this.messageACLID = messageACLID;
    }

    public void setControladorID(String controladorID) {
        this.controladorID = controladorID;
    }


    //INNERCLASS --------------------------------------
    private class MessageParameter {
        private String campo1;
        private String campo2;
        private String campo3;
        private String campo4;
        private String campo5;

        public String getCampo1() {
            return campo1;
        }

        public void setCampo1(String campo1) {
            this.campo1 = campo1;
        }

        public String getCampo2() {
            return campo2;
        }

        public void setCampo2(String campo2) {
            this.campo2 = campo2;
        }

        public String getCampo3() {
            return campo3;
        }

        public void setCampo3(String campo3) {
            this.campo3 = campo3;
        }

        public String getCampo4() {
            return campo4;
        }

        public void setCampo4(String campo4) {
            this.campo4 = campo4;
        }

        public String getCampo5() {
            return campo5;
        }

        public void setCampo5(String campo5) {
            this.campo5 = campo5;
        }
    }//END OF INNERCLASS ----------------------------------------


}


