
package Logic;
import kong.unirest.Unirest;
import org.apache.commons.codec.StringEncoder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;

import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Clase que maneja los servicios de conexion por HW
 * La responsabilidad de esta clase es la de gestionar la comunicacion contra las ESP32
 * Aqui se hace la conversiond el modelo de Agentes a un modelo basado en Client-Server
 */
public class GestorConexion {

    private static final java.lang.String URL_API_COM = "http://000.000.000.000:0000/api";

    public static int sendToAPI(String content) throws IOException, URISyntaxException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(new URI(URL_API_COM));

        httpPost.setEntity(new StringEntity(content));
        CloseableHttpResponse response = client.execute(httpPost);


        return response.getStatusLine().getStatusCode();



    }


}
