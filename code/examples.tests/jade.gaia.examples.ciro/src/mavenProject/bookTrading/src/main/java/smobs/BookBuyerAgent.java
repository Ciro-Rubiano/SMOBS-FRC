package smobs;

import jade.core.Agent;
import jade.core.AID;
import java.util.logging.Level;
import java.util.logging.Logger;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class BookBuyerAgent extends Agent{

    //atr
    /**
     * Creación del logger de la clase
     */
    private static final Logger LOGGER = Logger.getLogger("BookBuyerAgent");

    /**
     * Simplemente el Titulo del libro que este agente comprador está buscando.
     */
    private String targetBookTitle; 

    /**
     * Array que contiene los vendedores que este comprador conoce.
     * No estamos creando vendedores, simplemente sus referencias bajo esos nombres.
     * pueden estar o no en la plataforma
     */
    private AID[] sellersAgents;
    
    /**
     * Este metodo es el metodo de INICIALIZACION de un agente BUYER. NO es un constructor
     * (Aunque internamente lo construya)
     */
    @Override
    protected void setup(){
        /*
        LOGGER.info("AGENTE CREADO");
        System.out.println("##AGENT INFO##");
        System.out.println("BUYER NAME:" + getAID().getName());
        */
        //Vamos a buscar el servicio
        //vamos a crear un comportamiento del agente para buscarlos
        //cambiar este valor para cambiar el tickrate en secs
        final int TICKRATE = 30;
        
        addBehaviour(new TickerBehaviour(this, TICKRATE*1000){
            @Override
            protected void onTick() {

                DFAgentDescription templateAgent = new DFAgentDescription();
                ServiceDescription templateService = new ServiceDescription();
                templateService.setName("jade.book.trading"); //notar que debe ser igual que en la proposición del Seller
                templateAgent.addServices(templateService);
                
                try {
                    //vamos a buscarlo
                    DFAgentDescription[] encontrados = DFService.search(myAgent, templateAgent);
                    sellersAgents = new AID[encontrados.length];
                    for (int i = 0; i < encontrados.length; i++) {
                        sellersAgents[i] = encontrados[i].getName();
                        
                    }
                } catch (FIPAException ex) {
                    LOGGER.log(Level.SEVERE, "[EXCEP] Fallo al recuperar los Sellers", ex);
                }
                
                myAgent.addBehaviour(new RequestPerformer());

            }
            
            
            
        });
        
        
        
        //procesar argumentos
        if(!this.processArguments()) doDelete();
        //añadimos un ticker (daemon cada 10 secs)
        
    }
    /**
     * Procesamiento de los argumentos recibidos en el setup del agente.
     */
    private boolean processArguments(){

        Object[] args = getArguments();
        if(args != null && args.length > 0){
            this.targetBookTitle = (String) args[0];
            System.out.println("INTENTANDO COMPRAR: " +  this.targetBookTitle);
            return true;
        }else{
            //si no encontramos parametros el agente no tiene proposito, tenemos que terminarlo inmediatamente
            LOGGER.log(Level.WARNING, "EL AGENTE NO TIENE PROPOSITO, SERÁ ELIMINADO");
            //doDelete(); //metodo para indicar a un agente que debe ser eliminado.
            return false;
        }

    }

    /**
     * Metodo de limpieza de un agente.
     * Aqui se deberian especificar todas aquellas instrucciones necesarias para "resetear" un agente.s
     */
    @Override
    protected void takeDown(){
        LOGGER.log(Level.INFO, "BUYER: {0}-> TERMINATING...", getAID().getName());
        
    }

    //Behaviour 
    private class RequestPerformer extends Behaviour{

        private AID mejorVendedor;
        private int mejorPrecio;
        private int cantidadRespuestas = 0;
        private MessageTemplate messTemp;
        private final String IDCONVERSATION = "Book-Trade";
        private int step = 0;
        
        
        @Override
        public void action() {
            switch (step){
                case 0://PASO NUMERO 1
                    //ENVIAR EL CFP - a todos los Sellers
                    ACLMessage message = new ACLMessage(ACLMessage.CFP);
                    
                    //iteramos los agentes vendedores
                    for (AID sellerAgent : sellersAgents) {
                        LOGGER.log(Level.INFO, "[DEBUG]: Agente Iterado: {0}", sellerAgent.getName());
                        message.addReceiver(sellerAgent); //receptor
                        message.setContent(targetBookTitle);
                        message.setConversationId(IDCONVERSATION);
                        message.setReplyWith("cfp"+System.currentTimeMillis());//VALOR UNICO ?¿
                        
                        myAgent.send(message);
                    }
                    //preparamos la template para recibir
                    messTemp = MessageTemplate.and(MessageTemplate.MatchConversationId(IDCONVERSATION),
                               MessageTemplate.MatchInReplyTo(message.getReplyWith()));//especificamos dos Restricciones que debe tener el mensaje a recibir
                    step = 1;
                    break;
                    
                case 1: //PASO NUMERO 2
                    //recepcion de respuestas de los Sellers
                    ACLMessage reply = myAgent.receive(messTemp);//filtramos con la plantilla
                    
                    if(reply != null){
                        if(reply.getPerformative()==ACLMessage.PROPOSE){
                            //ahora sabemos que es una proposicion y que contiene el precio del libro
                            int precioLibro = Integer.parseInt(reply.getContent()); //guardamos el precio en una variable
                            
                            //ahora vamos a buscar el mejor vendedor
                            if(mejorVendedor==null || precioLibro < mejorPrecio){
                                
                                mejorPrecio = precioLibro; //dejamos el nuevo mejor precio
                                mejorVendedor = reply.getSender(); //guardamos el mejor vendedor (quien tiene el mejor precio)
                                
                            }
                            cantidadRespuestas++;
                        }
                        if(reply.getPerformative()==ACLMessage.REFUSE){
                            //el agente Seller no tiene el libro
                            cantidadRespuestas++;
                                                
                        }
                        if(cantidadRespuestas >= sellersAgents.length){
                            //tenemos todas las respuestas
                            step=2;
                        }
                        
                        
                    }else{
                        block();
                    }
                    break;
                    
                case 2: //PASO NUMERO 3
                    /*
                    En este paso lo que vamos a hacer es enviar una solicitud de compra a el mejor seller
                    */
                    //creamos un mensaje para enviar la solicitud
                    ACLMessage  orden = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
                    orden.addReceiver(mejorVendedor);
                    orden.setContent(targetBookTitle);
                    orden.setConversationId(IDCONVERSATION);
                    orden.setReplyWith("orden"+System.currentTimeMillis());
                    myAgent.send(orden);
                    
                    //como ya enviamos el mensaje, ahora vamos a preparar la template, para encontrar luego la respuesta.
                    
                    messTemp = MessageTemplate.and(MessageTemplate.MatchConversationId(IDCONVERSATION),
                                        MessageTemplate.MatchInReplyTo(orden.getReplyWith()));
                    step = 3;
                    break;
                    
                case 3: //PASO NUMERO 4
                    //recibimos la respuesta del pedido de compra
                    reply = myAgent.receive(messTemp);
                    //esta filtrada asi que ahora sabemos que es la respuesta del vendedor
                    if(reply!=null){
                        if(ACLMessage.INFORM==reply.getPerformative()){

                            //La compra se realizó con exito. El agente cumplió con su proposito
                            LOGGER.log(Level.INFO, "[INFO] La compra fue realizada con exito");
                            LOGGER.log(Level.INFO, "[INFO] Libro Comprado: "+ targetBookTitle+ " Precio comprado: " + mejorPrecio + "Comprado a: "+ mejorVendedor);
                            LOGGER.log(Level.INFO, "[INFO] El agente será eliminado...");

                            doDelete(); //Solicitamos la eliminación
                            step = 9;

                        }
                        
                    }else{block();}
                    
                    break;
                    
            }//CIERRE CASE

        }
        
        
       

        @Override
        public boolean done() {
            //para ver si el comportamiento fue exitoso, debemos ahora devolver un boolean controlando el step y cuales son los vendedores
            //si aun estamos en el paso 2, y no tenemos mejor vendedor, entonces no se recibieron respuestas de los Sellers
            //por otro lado, si estamos en el paso 9, significa que llegamos al final de la ejecución, y que por tanto la compra esta realizada
            return (step == 2 && mejorVendedor == null || step == 9);
        }
    
    
        
    }
    


}
