import Logic.GestorConexion;
import Models.MessageContent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import sun.plugin2.message.Message;

import javax.swing.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComunicadorAgente extends Agent {

    public Logger log = Logger.getAnonymousLogger();
    private ArrayList<String> ontologias = new ArrayList<String>();
    private int id_ESP; //

    // COMO MODIFICO ESTO DESDE EL BEHAVIOUR D:
    private ArrayList<String> porProcesar = new ArrayList<String>();


    private void setOntologies() {
        ontologias.add(OntologySMOBS.MEDIR_HUMEDAD.toString());
        ontologias.add(OntologySMOBS.MEDIR_HUMEDAD_SUELO.toString());
        ontologias.add(OntologySMOBS.MEDIR_TEMPERATURA.toString());
        ontologias.add(OntologySMOBS.MEDIR_TEMPERATURA_AMBIENTE.toString());
    }

    @Override
    protected void setup() {
        this.setOntologies();
        //registramos al agente y su comportaminto
        addBehaviour(new ObtenerMedicion());

        //Registramos el servicio en el DF
        DFAgentDescription dfDescription = new DFAgentDescription();
        dfDescription.setName(getAID());
        ServiceDescription sDescription = new ServiceDescription();
        sDescription.setType("Comunicacion Microcontrolador");
        sDescription.setName("Comunicador");
        dfDescription.addServices(sDescription);
        try {
            DFService.register(this, dfDescription);
        } catch (FIPAException fe) {
            fe.printStackTrace();

        }


    }

    @Deprecated
    private class ObtenerMedicionV1 extends CyclicBehaviour {

        public void action() {
            //PASO 1 - Esperar peticion de medicion
            //creamos la template
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
            MessageTemplate.MatchLanguage("application/json");

            ACLMessage message = myAgent.receive(mt);
            //message.setOntology(OntologySMOBS.ACCION.MEDIR.HUMEDAD.toString());
            ACLMessage reply = message.createReply();
            if (message != null) {

                String content = message.getContent();
                String replyWith = message.getReplyWith();

                //procesar el contenido
                //Models.MessageContent co = new Gson().fromJson(content, Models.MessageContent.class);
                //String idControlador = co.getControladorID();//no se usa en prueba

                String ont = message.getOntology();
                //contestar con mensaje de error
                if (true) {//COMUNICARSE CON ARDUINO
                    String rta = JOptionPane.showInputDialog(null, "Ingrese el valor para " + ont);

                    reply.setPerformative(ACLMessage.AGREE);
                    reply.setLanguage("text/plain");
                    reply.setContent(rta);
                }


                //respondemos
                myAgent.send(reply);

            } else {
                block();
                return;
            }


            //PASO 2 - Enviar peticion a ESP32

            //PASO 3 - Obtener respuesta de ESP32

            //PASO 4 - Responder a quien hizo la peticion de medicion

        }

    }

    private class ObtenerMedicion extends CyclicBehaviour {

        MessageTemplate template = (MessageTemplate.MatchPerformative(ACLMessage.REQUEST));

        public void action() {

            //ver si la request es valida

            ACLMessage mensaje = myAgent.receive();
            if (mensaje == null) {
                log.log(Level.INFO, myAgent.getLocalName() + "blocked");
                block();

            } else {
                log.log(Level.INFO, "unblocked");

                ACLMessage reply = mensaje.createReply();
                if (ontologias.contains(mensaje.getOntology())) {

                    /*MessageContent jsonMessageWOACLID = new Gson().fromJson(mensaje.getContent(), MessageContent.class);
                    ComunicadorAgente.PushToAPI(jsonMessageWOACLID);//TODO: aqui deberia enviar hacia la API
                    */

                    ComunicadorAgente.PushToAPI(mensaje.getContent());

                    //es entendido
                    reply.setPerformative(ACLMessage.AGREE);
                    reply.setOntology(OntologySMOBS.INFORMAR_OK.toString());
                    myAgent.send(reply);//TODO: Esto deberia enviarse una vez que la API devuelva 200

                    //como la peticion, se va a realizar, lo agregamos a la cola, de procesamientos pendientes
                    //Models.MessageContent peticion = new Gson().fromJson(mensaje.getContent(), Models.MessageContent.class);

                    porProcesar.add(mensaje.getContent());

                    //HARDCODEO HEDIONDO
                    SimuladorArduino(mensaje, reply);


                    //que quiere el solicitante?


                    //Chequeamos conexion con el arduino (PODEMOS CUMPLIR?)


                } else {
                    //mensaje no entendido
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setLanguage("plain/text");
                    reply.setOntology(OntologySMOBS.INFORMAR_ERROR.toString());
                    reply.setContent("No se reconoce la ontologia, es posible que desee comunicarse con otro agente");
                    myAgent.send(reply);
                }


            }


        }

        public void SimuladorArduino(ACLMessage mensaje, ACLMessage reply) {

            //HARDCODEO SIMULANDO ARDUINO
            // estas no son responsabilidades de comunicador, el solo debe ver si entiende la encologia , entonces encola el mensaje
            // no necesita saber que hacer con eso , eso corresponde al arduino que va a consumir el mensaje encolado

            // Puedo realizar la accion ?
            int rta1 = JOptionPane.showConfirmDialog(null, mensaje.getContent()
                    + "REQ: " + mensaje.getOntology(), "INFORMACION", JOptionPane.YES_NO_OPTION);


            if (rta1 == 0) {
                //el arduino puede realizar la accion
                String resultado_Arduino = JOptionPane.showInputDialog("Ingrese la respuesta de la medicion del arduino");
                reply.setContent(resultado_Arduino);
                reply.setInReplyTo(mensaje.getReplyWith());
                reply.setPerformative(ACLMessage.INFORM);
                reply.setOntology(OntologySMOBS.INFORMAR_REALIZADO_CORRECTAMENTE.toString());
                myAgent.send(reply);


            } else {
                //El arduino no puede realizar la accion
                int rta2 = JOptionPane.showConfirmDialog(null, "FALLÓ EN LA TAREA?", "NO PUDO REALIZAR LA ACCION", JOptionPane.YES_NO_OPTION);
                if(rta2==0){
                    reply.setPerformative(ACLMessage.FAILURE);
                    reply.setContent("LA TAREA FALLO DURANTE SU EJECUCION, EL ARDUINO, ES UN MISERABLE");
                    reply.setOntology(OntologySMOBS.INFORMAR_ERROR.toString());
                    myAgent.send(reply);
                }

            }


        }


    }

    /**
     * Concatena hacia el GestorConexion.java
     * @param mensajeAArduino lo que va a recibir el arduino
     * @return el codigo HTTP de la solicitud
     */
    private static int PushToAPI(String mensajeAArduino) {
        try {
            return GestorConexion.sendToAPI(mensajeAArduino);
        } catch (Exception e) {
            e.printStackTrace();
            return 500;
        }

    }

}


